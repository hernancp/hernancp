package combinacion de vectores;
public /**
 * combinacion de vectores
 */
public class combinacion de vectores {
    static final int tam = 5;
    static final int tamr= 10;
    
    public static void main(String[] args) {
        int vu[]= new int[tam];
        int vd[]= new int[tam];
        int vr[]= new int[tamr];
        int n=tam;
        //se llena el primer vector con numeros aleatorios
        llenarVector(vu,tam,1,100);
        System.out.println ("\nPrimer Vector");
        mostrarVector(vu,n);

        //se llena el segundo vector con numeros aleatorios
        llenarVector(vd,tam,1,100);
        System.out.println ("\nSegundo Vector");
        mostrarVector(vd,n);

        //el vector resultante, se obtiene del intercalado
        int i=0,k,n=tam;
        for (k=0; k<n; k++) {
                vr[i] =vu[k];
                vr[i+1] = vd[k];
                i= i + 2;
        }
        limpiarVector(vu,n,0);
        limpiarVector(vd,n,0);

        //mostrar vector resultante
        System.out.println ("\nVector Resultante");
        mostrarVector(vr,tamr);
        
        System.out.println ("\nPrimer Resultante");
        mostrarVector(vu,tamr);
        
        System.out.println ("\nSegundo Vector");
        mostrarVector(vd,tamr);

    }//fin del metodo principal

    //metodo para llenar un vector aleatoriamente
    //se recibe el vector(v), el tamaño (n), VI, VF

    static void llenarVector(int v[], int n, int VI, int VF) {
        int k;
        Random generador = new Random();
        for (k=0; k<n; k++) {
            v[k] =generador.nextInt (VF) + VI;
        
        }//fin de metodo

        static void limpiarVector (int v[],int n, int valor) {
            int k;
            for (k=0 ; k<n; k++) {
                    v[k] = valor;
            }
        }
    static void mostrarVector (int v[],int n, int valor) {
        int k;
        for (k=0; k<n; k++) {
            System.out.print ("[" +v[k] + "]");
        }
    }//fin del metodo mostrarvector
    
    }//fin de la clase

}